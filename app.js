'use strict';

// Create an instance
var wavesurfer = {};

// Init & load audio file
document.addEventListener('DOMContentLoaded', function() {
    wavesurfer = WaveSurfer.create({
        container: document.querySelector('#waveform'),
        plugins: [
            WaveSurfer.cursor.create({
                showTime: true,
                opacity: 1,
                customShowTimeStyle: {
                    'background-color': '#000',
                    color: '#fff',
                    padding: '2px',
                    'font-size': '10px'
                }
            })
        ]
    });

    wavesurfer.on('ready', function () {
    	showTime();
    	$(".duration").text(" / "+wavesurfer.getDuration().toFixed(1));
        $(".currentTime").removeClass("blink");
	    //wavesurfer.play();
	    console.log("audio loaded and rendered");
	});

    wavesurfer.on('error', function(e) {
        console.warn(e);
    });

    wavesurfer.on('seek', function () {
    	showTime();
	    console.log(wavesurfer.getCurrentTime());
	});

    wavesurfer.on('audioprocess', function () {
    	showTime();
	});

    // show progress while loading sound
    wavesurfer.on('loading', function(percentage, evt) {
        if (percentage < 100) {
            $(".currentTime").html("... loading "+percentage+" % ...");
        } 
        else {
            $(".currentTime").html("... decoding ...");
        }
    });

    // Load audio from URL
    //wavesurfer.load('RadioPajaros_42_lasMANOS_VictoriaSantaCruzPanderoCuadrado.mp3');
    //wavesurfer.load('https://xrcb.cat/wp-content/uploads/2019/07/RadioPajaros_42_lasMANOS_VictoriaSantaCruzPanderoCuadrado.mp3');
    wavesurfer.load('demo.mp3');
    clearTime();

    // Play button
    var button = document.querySelector('[data-action="play"]');

    $(".btn-play").click(function() {
    	wavesurfer.playPause();
    });

    $(".btn-demo").click(function() {
    	wavesurfer.load('demo.mp3');
    	clearTime();
    });

    $(".btn-podcast").click(function() {
    	wavesurfer.load('https://xrcb.cat/wp-content/uploads/2019/07/RadioPajaros_42_lasMANOS_VictoriaSantaCruzPanderoCuadrado.mp3');
    	clearTime();
    });

    function showTime() {
	    $(".currentTime").text(wavesurfer.getCurrentTime().toFixed(1));
    }

    function clearTime() {
    	$(".duration").text("");
	    $(".currentTime").addClass("blink");
    }
});
